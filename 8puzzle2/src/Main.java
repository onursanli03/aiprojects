import java.util.Scanner;

public class Main {

    final static private String problem1 = "724506831";
    final static private String problem2 = "125340678";
    final static private String problem3 = "142658730";
    final static private String problem4 = "102754863";
    final static private String GOAL_STATE = "012345678";

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String rootState = "";
        int selectProblem;
        long startTime = System.currentTimeMillis();
        System.out.println("Select Problem :");
        System.out.println("1-problem 1"
                + "\n"
                + "2-problem 2"
                + "\n"
                + "3-problem 3"
                + "\n"
                + "4-problem 4");
        while(sc.hasNext()) {
            selectProblem = sc.nextInt();
            switch (selectProblem) {
                case 1:
                    rootState = problem1;
                    System.out.println("Initial State of Problem :");
                    System.out.println("*******\n" +
                            "* 724 *\n" +
                            "* 506 *\n" +
                            "* 831 *\n" +
                            "*******");
                    break;
                case 2:
                    rootState = problem2;
                    System.out.println("Initial State of Problem :");
                    System.out.println("*******\n" +
                            "* 125 *\n" +
                            "* 340 *\n" +
                            "* 678 *\n" +
                            "*******");
                    break;
                case 3:
                    rootState = problem3;
                    System.out.println("Initial State of Problem :");
                    System.out.println("*******\n" +
                            "* 142 *\n" +
                            "* 658 *\n" +
                            "* 730 *\n" +
                            "*******");
                    break;
                case 4:
                    rootState = problem4;
                    System.out.println("Initial State of Problem :");
                    System.out.println("*******\n" +
                            "* 102 *\n" +
                            "* 754 *\n" +
                            "* 863 *\n" +
                            "*******");
                    break;
                default:
                    System.out.println("incorrect entry");
                    break;
            }
            if (selectProblem<5){
                break;
            }else{
                continue;
            }

        }
        SearchTree search = new SearchTree(new Node(rootState), GOAL_STATE);

        int selectAlgorithm;
        System.out.println("Please select search Algorithm");
        System.out.println("1- Breadth First Search"
                + "\n"
                + "2- Depth First Search"
                + "\n"
                + "3- Iterative Deepening Search"
                + "\n"
                + "4- Best First Search"
                + "\n"
                + "5- Uniform Cost Search"
                + "\n"
                + "6- A* Search"
                + "\n");
        while(sc.hasNext()) {

            selectAlgorithm = sc.nextInt();

            switch (selectAlgorithm) {
                case 1:
                    search.breadthFirstSearch();
                    break;
                case 2:
                    search.depthFirstSearch();
                    break;
                case 3:
                    search.iterativeDeepening(9);
                    break;
                case 4:
                    search.bestFirstSearch();
                    break;
                case 5:
                    search.uniformCostSearch();
                    break;
                case 6:
                    search.aStar(Heuristic.H_ONE);
                    break;
                default:
                    System.out.println("incorrect entry");
                    break;
            }
            if (selectAlgorithm<7){
                break;
            }else{
                continue;
            }
        }


        long finishTime = System.currentTimeMillis();
        long totalTime = finishTime - startTime;
        System.out.println("Time  :" + totalTime);


    }


}
