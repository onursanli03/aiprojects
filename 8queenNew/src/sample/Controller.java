

/*
 * Copyright (c) 2018. Created by Onur Şanlı
 */

package sample;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;


import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;


public class Controller {


    @FXML
    private JFXButton btnClose;
    @FXML
    private JFXButton btnMinimize;
    @FXML
    private AnchorPane frame;

    @FXML
    private BorderPane mainWordPane;

    @FXML
    private Label noSolutionLabel;

    @FXML
    private TextField hcIterations;

    @FXML
    private TextField queens;

    @FXML
    private SplitPane splitWordList;


    private double initX, initY;


    @FXML
    private void minimizeAction(ActionEvent evt) {
        Stage stage = (Stage) btnMinimize.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void closeAction(ActionEvent evt) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void movePressed(MouseEvent evt) {
        Stage stage = (Stage) frame.getScene().getWindow();
        initX = evt.getSceneX();
        initY = evt.getSceneY();
    }

    @FXML
    private void moveDragged(MouseEvent evt) {
        Stage stage = (Stage) frame.getScene().getWindow();
        stage.setX(evt.getScreenX() - initX);
        stage.setY(evt.getScreenY() - initY);

    }

    GUIPuzzle guiPuzzle = new GUIPuzzle();

    private int getNum(TextField tf) {
        return Integer.parseInt(tf.getText());
    }

    private double getDouble(TextField tf) {
        return Double.parseDouble(tf.getText());
    }

    @FXML
    void runHillClimbing(ActionEvent event) {
        HillClimbing hillClimbing = new HillClimbing();
        int[] res = hillClimbing.solve(getNum(queens), getNum(hcIterations));

        showResult(res);
    }


    private void showResult(int[] res) {
        guiPuzzle.drawQueens(res);
        noSolutionLabel.setVisible(res == null);
    }


    @FXML
    void initialize() {
        assert mainWordPane != null : "fx:id=\"mainWordPane\" was not injected: check your FXML file 'sample.fxml'.";
        assert hcIterations != null : "fx:id=\"hcIterations\" was not injected: check your FXML file 'sample.fxml'.";
        assert queens != null : "fx:id=\"queens\" was not injected: check your FXML file 'sample.fxml'.";
        assert splitWordList != null : "fx:id=\"splitWordList\" was not injected: check your FXML file 'sample.fxml'.";


        mainWordPane.setCenter(guiPuzzle);
        mainWordPane.setBackground(new Background(new BackgroundFill(Color.ANTIQUEWHITE, CornerRadii.EMPTY, new Insets(0, 0, 0, 0))));

    }


}
