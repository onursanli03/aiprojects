package sample;

public interface SolverStrategy {

    public int[] solve();
}
